--Enemy is a vector, keyboard movable, drawable, entity
Enemy = {}

function Enemy:new()
	local newEnemy = {}
	setmetatable(newEnemy, self)
	self.__index = self
	
	newEnemy.mover = Mover:new()
	newEnemy.hitBox = HitBox:new()

	return newEnemy

end

function Enemy:draw()

	self.hitBox:draw()
end

function Enemy:update(inputHandler)

end
