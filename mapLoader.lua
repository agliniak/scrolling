MapLoader = {}

function MapLoader:new(renderStack)
	local newMapLoader = {}
	setmetatable(newMapLoader, self)
	self.__index = self

	newMapLoader.newMapLoader = renderStack
	--later have mapviewcontroller that will take the map 
	--and decide what to send to renderstack
	newMapLoader.map = {}
	newMapLoader.map.image = 0
	newMapLoader.map.quads = {}
	newMapLoader.map.tiledData = 0


	
	return newMapLoader
end

function MapLoader:load(fileName)

	--load tiled file into object
	self.map.tiledData = love.filesystem.load(fileName)()

	--pull values out so its easier to use and read
	self.mapWidthQuads = self.map.tiledData.width
	self.mapHeightQuads = self.map.tiledData.height

	self.map.tilesetImage = love.graphics.newImage(self.map.tiledData.tilesets[1].image)
	self.tilesetWidth = self.map.tiledData.tilesets[1].imagewidth
	self.tilesetHeight = self.map.tiledData.tilesets[1].imageheight
	self.tileWidth = self.map.tiledData.tilewidth
	self.tileHeight = self.map.tiledData.tileheight
	
	
	self.quadWidth = self.tilesetWidth/self.tileWidth
	self.quadHeight = self.tilesetHeight/self.tileHeight
	self.quadTotal = self.quadWidth*self.quadHeight

	self:MakeQuads()

	print("MapLoader - Quads: "..#self.map.quads)



	--dostuff
	--return a map object that has a draw

end

--
function MapLoader:MakeQuads()

	local quadIndex = 1 

	for  rowIndex = 0, self.quadHeight-1 do

		for  columnIndex = 0, self.quadWidth-1 do

			self.map.quads[quadIndex] = love.graphics.newQuad(columnIndex*self.tileWidth,rowIndex*self.tileHeight,
													 self.tileWidth,self.tileHeight,
													 self.tilesetWidth,self.tilesetHeight)
			
			quadIndex=quadIndex+1
			print(quadIndex)
		end
	end

end